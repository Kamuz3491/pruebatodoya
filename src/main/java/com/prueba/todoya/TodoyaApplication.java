package com.prueba.todoya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoyaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoyaApplication.class, args);
	}

}
