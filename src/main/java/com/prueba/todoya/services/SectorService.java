package com.prueba.todoya.services;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.todoya.models.SectorsModel;
import com.prueba.todoya.repositories.SectorsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SectorService {

    @Autowired
    SectorsRepository sectorRepository;

    public ArrayList<SectorsModel> obtenerSectores(){
        return (ArrayList<SectorsModel>) sectorRepository.findAll();
    }

    public SectorsModel guardarSector(SectorsModel sector){
        return sectorRepository.save(sector);
    }

    public Optional<SectorsModel> obtenerPorId(Integer id){
        return sectorRepository.findById(id);
    }
    
    public boolean eliminarSector(Integer id){
        try{
            sectorRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
