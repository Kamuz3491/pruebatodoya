package com.prueba.todoya.services;

import java.util.ArrayList;

import com.prueba.todoya.models.ClientsModel;
import com.prueba.todoya.repositories.ClientsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    ClientsRepository clientsRepository;

    public ArrayList<ClientsModel> obtenerClientes(){
        return (ArrayList<ClientsModel>) clientsRepository.findAll();
    }

    public ClientsModel guardarCliente(ClientsModel cliente){
        return clientsRepository.save(cliente);
    }

    public boolean eliminarCliente(Integer id){
        try{
            clientsRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
