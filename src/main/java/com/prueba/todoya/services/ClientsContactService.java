package com.prueba.todoya.services;

import java.util.ArrayList;

import com.prueba.todoya.models.ClientsContactsModel;
import com.prueba.todoya.repositories.ClientsContactsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientsContactService {

    @Autowired
    ClientsContactsRepository clientsContactsRepository;

    public ArrayList<ClientsContactsModel> obtenerContactosCliente(){
        return (ArrayList<ClientsContactsModel>) clientsContactsRepository.findAll();
    }

    public ClientsContactsModel guardarContactoCliente(ClientsContactsModel contactoCliente){
        return clientsContactsRepository.save(contactoCliente);
    }

    public boolean eliminarContactoCliente(Integer id){
        try{
            clientsContactsRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
