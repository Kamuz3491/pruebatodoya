package com.prueba.todoya.services;

import java.util.ArrayList;
// import java.util.List;

import com.prueba.todoya.models.UserModel;
import com.prueba.todoya.repositories.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.GrantedAuthority;
// import org.springframework.security.core.authority.SimpleGrantedAuthority;
// import org.springframework.security.core.userdetails.User;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UserDetailsService;
// import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

// public class UserService implements UserDetailsService
@Service
public class UserService {

    @Autowired
    UsersRepository usersRepository;


    public ArrayList<UserModel> obtenerUsuarios(){
        return (ArrayList<UserModel>) usersRepository.findAll();
    }

    public UserModel guardarUsuario(UserModel user){
        return usersRepository.save(user);
    }

    // @Override
    // public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    //     UserModel usuario = usersRepository.findByNombre(username);

    //     List<GrantedAuthority> roles = new ArrayList<>();
    //     roles.add(new SimpleGrantedAuthority("ADMIN"));

    //     // UserDetails userDet = new User(usuario.getName(), usuario.getPass());
    //     UserDetails userDet = new User(usuario.getName(), usuario.getPass(), roles);
    //     return userDet;
    // }

    public boolean eliminarUsuario(Integer id){
        try{
            usersRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
