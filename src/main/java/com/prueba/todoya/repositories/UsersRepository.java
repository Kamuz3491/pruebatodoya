package com.prueba.todoya.repositories;

import com.prueba.todoya.models.UserModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<UserModel, Integer> {

    // UserModel findByNombre(String name);
    
}
