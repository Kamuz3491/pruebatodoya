package com.prueba.todoya.repositories;

import com.prueba.todoya.models.ClientsModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientsRepository extends CrudRepository<ClientsModel, Integer>{
    
}
