package com.prueba.todoya.repositories;

import com.prueba.todoya.models.SectorsModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorsRepository extends CrudRepository<SectorsModel, Integer>{

}