package com.prueba.todoya.repositories;

import com.prueba.todoya.models.ClientsContactsModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientsContactsRepository extends CrudRepository<ClientsContactsModel, Integer>{
    
}
