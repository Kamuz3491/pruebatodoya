package com.prueba.todoya.controllers;

import java.util.ArrayList;

import com.prueba.todoya.models.UserModel;
import com.prueba.todoya.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping()
    public ArrayList<UserModel> mostrarUsuarios(){
        return userService.obtenerUsuarios();
    }

    @PostMapping()
    public UserModel insertarUsuario(@RequestBody UserModel user){
        return this.userService.guardarUsuario(user);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Integer id){
        boolean ok = this.userService.eliminarUsuario(id);
        if(ok){
            return "Se elimino el usuario " + id;
        } else{
            return "No se pudo eliminar el usuario " + id;
        }
    }
    
}
