package com.prueba.todoya.controllers;

import java.util.ArrayList;
import java.util.Optional;

import com.prueba.todoya.models.SectorsModel;
import com.prueba.todoya.services.SectorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sector")
public class SectorController {

    @Autowired
    SectorService sectorService;

    @GetMapping()
    public ArrayList<SectorsModel> mostrarSectores(){
        return sectorService.obtenerSectores();
    }

    @PostMapping()
    public SectorsModel insertarSector(@RequestBody SectorsModel sector){
        return this.sectorService.guardarSector(sector);
    }

    @GetMapping(path = "/{id}")
    public Optional<SectorsModel> obtenerSectorPorId(@PathVariable("id") Integer id){
        return this.sectorService.obtenerPorId(id);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Integer id){
        boolean ok = this.sectorService.eliminarSector(id);
        if(ok){
            return "Se elimino el sector " + id;
        } else{
            return "No se pudo eliminar el sector " + id;
        }
    }
}
