package com.prueba.todoya.controllers;

import java.util.ArrayList;

import com.prueba.todoya.models.ClientsModel;
import com.prueba.todoya.services.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientsController {

    @Autowired
    ClientService clientService;

    @GetMapping()
    public ArrayList<ClientsModel> mostrarClientes(){
        return clientService.obtenerClientes();
    }

    @PostMapping
    public ClientsModel insertarCliente(@RequestBody ClientsModel cliente){
        return this.clientService.guardarCliente(cliente);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Integer id){
        boolean ok = this.clientService.eliminarCliente(id);
        if(ok){
            return "Se elimino el cliente " + id;
        } else{
            return "No se pudo eliminar el cliente " + id;
        }
    }
    
}
