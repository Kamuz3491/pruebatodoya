package com.prueba.todoya.controllers;

import java.util.ArrayList;

import com.prueba.todoya.models.ClientsContactsModel;
import com.prueba.todoya.services.ClientsContactService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public class ClientsContactsController {

    @Autowired
    ClientsContactService clientsContactService;

    @GetMapping
    public ArrayList<ClientsContactsModel> mostrarContactosCliente(){
        return clientsContactService.obtenerContactosCliente();
    }

    @PostMapping
    public ClientsContactsModel insertarContactoCliente(@RequestBody ClientsContactsModel contactClient){
        return this.clientsContactService.guardarContactoCliente(contactClient);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Integer id){
        boolean ok = this.clientsContactService.eliminarContactoCliente(id);
        if(ok){
            return "Se elimino el contacto " + id;
        } else{
            return "No se pudo eliminar el contacto " + id;
        }
    }
    
}
