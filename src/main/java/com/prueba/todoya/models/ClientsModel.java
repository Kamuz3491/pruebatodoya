package com.prueba.todoya.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "hire_clients")
public class ClientsModel {

    @Id
    private Integer id;
    private String name;
    private String email;
    private String nit;
    private String address;
    private String city;

    @ManyToOne
    @JoinColumn(name = "sector_id", referencedColumnName = "id")
    private SectorsModel sectorId;
    private String createdDate;

    @ManyToOne
    @JoinColumn(name = "id_created" , referencedColumnName = "id")
    private UserModel idCreated;


    @OneToMany(mappedBy = "clientId")
    private List<ClientsContactsModel> clientId;

    public static void main(String[] args) {

        SimpleDateFormat date = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss");
        String lastLogin = date.format(new Date());
        System.out.println("Current Time Stamp: "+lastLogin);
    }


    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getNit(){
        return nit;
    }

    public void setNit(String nit){
        this.nit = nit;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }

    public SectorsModel getSectorId(){
        return sectorId;
    }

    public void setSectorId(SectorsModel sectorId){
        this.sectorId = sectorId;
    }

    public String getCreatedDate(){
        return createdDate;
    }

    public void setCreatedDate(String createdDate){
        this.createdDate = createdDate;
    }

    public UserModel getIdCreated(){
        return idCreated;
    }

    public void setIdCreated(UserModel idCreated){
        this.idCreated = idCreated;
    }
    
}
