package com.prueba.todoya.models;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "hire_sectors")
public class SectorsModel {
    
    @Id
    @Column(name="id")
    private Integer id;
    private String sectorName;

    @OneToMany(mappedBy = "sectorId")
    private List<ClientsModel> sectorId;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getSectorName(){
        return sectorName;
    }

    public void setSectorName(String sectorName){
        this.sectorName = sectorName;
    }
}
