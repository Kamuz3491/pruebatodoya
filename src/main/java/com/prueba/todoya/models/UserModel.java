package com.prueba.todoya.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "hire_user")
public class UserModel {

    @Id
    @Column(name="id")
    private Integer id;
    private String name;
    private String lastName;
    private String email;
    private String pass;
    private String lastLogin;

    @OneToMany(mappedBy = "idCreated")
    private List<ClientsModel> idCreated;

    public static void main(String[] args) {
        SimpleDateFormat date = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss");
        String lastLogin = date.format(new Date());
        System.out.println("Current Time Stamp: "+lastLogin);
    }

    
    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPass(){
        return pass;
    }

    public void setPass(String pass){
        this.pass = pass;
    }

    public String getLastLogin(){
        return lastLogin;
    }

    public void setLastLogin(String lastLogin){
        this.lastLogin = lastLogin;
    }
}
