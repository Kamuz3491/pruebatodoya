package com.prueba.todoya.models;

import javax.persistence.*;

@Entity
@Table(name = "hire_clients_contacts")
public class ClientsContactsModel {

    @Id
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private ClientsModel clientId;

    private String name;
    private String email;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public ClientsModel getClientId(){
        return clientId;
    }

    public void setClientId(ClientsModel clientId){
        this.clientId = clientId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    
}
